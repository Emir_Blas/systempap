// VARIABLES INICIALES
const CACHE_NAME = "pwa-cache-offline";
const CACHE_STATIC_NAME = "pwa-static-v1";
const CACHE_DYNAMIC_NAME = "pwa-dynamic-v1";
const CACHE_INMUTABLE_NAME = "pwa-inmutable-v1";
const CACHE_DYNAMIC_LIMIT = 50;

var filesOffline = [
    "/offline.html",
    "/css/remixicon.css",
    "/css/styles.css",
    "/js/scrollreveal.min.js",
    "/js/app.js",
    "your-logo-72.png",
    "your-logo-96.png",
    "your-logo-128.png",
    "your-logo-152.png",
    "your-logo-192.png",
    "your-logo-384.png",
    "your-logo-512.png",
];

var appShell = [
    "/css/style.bundle.css",
    "/css/datatables.bundle.css",
    "/css/app.css",
    "/js/jquery.dataTables.min.js",
    "/js/search.js",
    "/js/icons.min.js",
    "/js/plugins.bundle.js",
    "/img/ARlogo2.png",
];

var inmutable = [
    "/css/fonts/bootstrap-icons/bootstrap-icon.woff",
    "/css/fonts/bootstrap-icons/bootstrap-icon.woff2",
];

function clearCache(CACHE_NAME, NUM_ITEMS) {
    caches.open(CACHE_NAME).then(async (cache) => {
        const keys = await cache.keys();
        if (keys.length > NUM_ITEMS) {
            cache.delete(keys[0]).then(clearCache(CACHE_NAME, NUM_ITEMS));
        }
    });
}

self.addEventListener("install", (event) => {
    this.skipWaiting();
    event.waitUntil(
        caches.open(CACHE_NAME).then((cache) => {
            return cache.addAll(filesOffline);
        }),
        caches.open(CACHE_STATIC_NAME).then((cache) => {
            return cache.addAll(appShell);
        }),
        caches.open(CACHE_INMUTABLE_NAME).then((cache) => {
            return cache.addAll(inmutable);
        })
    );
});

self.addEventListener("activate", (event) => {
    event.waitUntil(
        caches.keys().then((keys) => {
            keys.forEach((key) => {
                if (key !== CACHE_NAME && key.includes("pwa-cache-offline")) {
                    return caches.delete(key);
                }
                if (
                    key !== CACHE_STATIC_NAME &&
                    key.includes("pwa-static-v1")
                ) {
                    return caches.delete(key);
                }
            });
        })
    );
});

self.addEventListener("fetch", (event) => {
    if (!(event.request.url.indexOf("http") === 0)) return;
    event.respondWith(
        caches
            .match(event.request)
            .then((response) => {
                if (response) return response;
                return fetch(event.request).then((newResponse) => {
                    caches.open(CACHE_DYNAMIC_NAME).then((cache) => {
                        cache.put(event.request, newResponse);
                        clearCache(CACHE_DYNAMIC_NAME, CACHE_DYNAMIC_LIMIT);
                    });
                    return newResponse.clone();
                });
            })
            .catch(() => {
                return caches.match("/offline.html");
            })
    );
});