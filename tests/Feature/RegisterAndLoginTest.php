<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RegisterAndLoginTest extends TestCase
{
    public function test_register_to_a_user()
    {
        $this->get('/register')->assertSee('Register');
        $credentials = [
            "name" => "josue",
            "email" => "josue@gmail.com",
            "password" => "josue123",
            "password_confirmation" => "josue123"
        ];

        $response = $this->post('/register', $credentials);
        $this->assertCredentials($credentials);
    }

     public function test_authenticated_to_a_user()
    {
        $this->get('/login')->assertSee('Login');
        $credentials = [
            "name" => "josue",
            "password" => "josue123"
        ];

        $response = $this->post('/login', $credentials);
        $response->assertRedirect('/dashboard');
        $this->assertCredentials($credentials);
    }
}
