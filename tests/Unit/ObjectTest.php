<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Objects;
use App\Models\Category;

class ObjectTest extends TestCase
{
    public function test_object_belongs_to_category()
    {
        $object = Objects::find(1);
        $this->assertInstanceOf(Category::class, $object->category);
    }
}
