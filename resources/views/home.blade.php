@extends('layouts.home')

@section('title', 'Dashboard')

@section('title-nav', 'Dashboard')

@section('content')
    <div class="card bgi-position-y-bottom bgi-position-x-end bgi-no-repeat bgi-size-cover min-h-250px bg-primary mb-5 mb-xl-8"
        style="background-position: 100% 50px;background-size: 500px auto;background-image:url('img/city.png')">
        <!--begin::Body-->
        <div class="card-body d-flex flex-column justify-content-center ps-lg-12">
            <!--begin::Title-->
            <h3 class="text-white fs-2qx fw-bolder mb-7">Estamos trabajando
                <br>para brindarte una mejor experiencia
            </h3>
            <!--end::Title-->
            <!--begin::Action-->
            <div class="m-0">
                <a href="#" class="btn btn-success fw-bold px-6 py-3" data-bs-toggle="modal"
                    data-bs-target="#kt_modal_create_app">Ver información</a>
            </div>
            <!--begin::Action-->
        </div>
        <!--end::Body-->
    </div>
@endsection
