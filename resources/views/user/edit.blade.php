@extends('layouts.default')

@section('title','ARLAB - Editar usuario')

@section('content')

<!-- Page Content -->
<div class="content">
    <div class="col-md-12 col-xl-12">
        <a href="{{ route('users')}}">
            <button type="button" class="btn btn-alt-danger min-width-125 ">Cancelar</button>
        </a>
    </div>
</div>
<div class="content">
    <div class="col-md-12 col-xl-12">
        <!-- Progress Wizard -->
        <div class="js-wizard-simple block">
            <div class="block-content">
                <h3>Editar usuario</h3>
                <p>Edita un usuario para mostrarse en el sistema.</p>
            </div>
            <!-- Step Tabs -->
            <ul class="nav nav-tabs nav-tabs-block nav-fill" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="#wizard-simple-step1" data-toggle="tab">1. Usuario</a>
                </li>
                {{-- <li class="nav-item">
                    <a class="nav-link" href="#wizard-simple-step2" data-toggle="tab">2. Descripción</a>
                </li> --}}
                <li class="nav-item">
                    <a class="nav-link" href="#wizard-simple-step3" data-toggle="tab">2. Validación</a>
                </li>
            </ul>
            <!-- END Step Tabs -->

            <!-- Form -->
            <form action="{{ route('users.update', ['id'=>$users->id]) }}" method="POST">
                @method('PUT')
                @csrf
                <!-- Steps Content -->
                <div class="block-content block-content-full tab-content" style="min-height: 265px;">
                    <!-- Step 1 -->
                    <div class="tab-pane active" id="wizard-simple-step1" role="tabpanel">
                        <div class="form-group">
                            <label for="name">Nombre de usuario</label>
                            <input class="form-control" type="text" id="name" name="name" value="{{ $users->name }}" required>
                        </div>
                        {{-- <div class="form-group">
                            <label for="email">Correo electrónico</label>
                            <input class="form-control" type="text" id="email" name="email" value="{{ $users->email }}" required>
                        </div> --}}
                    </div>
                    <!-- END Step 1 -->

                    <!-- Step 2 -->
                    {{-- <div class="tab-pane" id="wizard-simple-step2" role="tabpanel">
                        <div class="form-group">
                            <label for="description">Descripción sobre la categoria</label>
                            <textarea class="form-control" id="description" name="description" rows="9" required> {{ $categories->description }} </textarea>
                        </div>
                    </div> --}}
                    <!-- END Step 2 -->

                    <!-- Step 3 -->
                    <div class="tab-pane" id="wizard-simple-step3" role="tabpanel">
                        <div class="form-group">
                            <label for="made_user">Para el usuario:</label>
                            <input class="form-control" type="hidden" id="made_user" name="made_user" value="{{Auth::user()->name}}" required>
                        </div>
                        <div class="block-content">
                            <p>*Se guarda el usuario que ha realizado la edición de este registro con fines de seguridad y para una gestion adecuada
                                de la información que se integra al sistema.</p>
                                <p>*Recuerda llenar todos los campos solicitados, de lo contrario no podra hacer el registro.</p>
                            {{-- <h5>*Se guarda el usuario que ha realizado la creación de este registro con fines de seguridad y para una gestion adecuada
                                de la información que se integra al sistema.
                            </h5> --}}
                        </div>
                        <div class="block-content">
                            <label for="active"></label>
                            <input class="form-control" type="hidden" id="active" name="active" value="1" required>
                            <label for="inactive"></label>
                            <input class="form-control" type="hidden" id="inactive" name="inactive" value="0" required>
                        </div>
                        <div class="col-12 text-right">
                            <button type="submit" class="btn btn-alt-success">
                                <i class="fa fa-check mr-5"></i> Actualizar
                            </button>
                        </div>
                    </div>
                    <!-- END Step 3 -->
                </div>
                <!-- END Steps Content -->
            </form>
            <!-- END Form -->
        </div>
        <!-- END Progress Wizard -->
    </div>
</div>
<!-- END Page Content -->
</main>

<!-- Footer -->
<footer id="page-footer" class="opacity-0">
    <div class="content py-20 font-size-xs clearfix">
        <!-- <div class="float-right">
            Crafted with <i class="fa fa-heart text-pulse"></i> by <a class="font-w600" href="https://1.envato.market/ydb" target="_blank">pixelcave</a>
        </div> -->
        <div class="float-left">
            <a class="font-w600" href="" target="_blank">ARLAB</a> &copy; <span class="js-year-copy"></span>
        </div>
    </div>
</footer>
<!-- END Footer -->
@endsection