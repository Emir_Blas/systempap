@extends('layouts.home')

@section('title','ARLAB - Crear usuario')

@section('content')
    <br><br><br><br><br>
    <ol class="breadcrumb text-muted fs-6 fw-bold">
        <li class="breadcrumb-item pe-3"><a href="{{ route('dashboard') }}" class="pe-3">Inicio</a></li>
        <li class="breadcrumb-item pe-3"><a href="{{ route('users') }}" class="pe-3">Usuarios</a></li>
        <li class="breadcrumb-item px-3 ">Nuevo objeto</li>
    </ol><br><br>
    <!--begin::Form-->
    <form class="form" action="{{ route('users.store') }}" method="POST">
        @csrf
        <!--begin::Input group-->
        <div class="fv-row mb-10">
            <!--begin::Label-->
            <label class="required fw-bold fs-6 mb-2 ">Nombre del usuario</label>
            <!--end::Label-->

            <!--begin::Input-->
            <input type="text" name="name" class="form-control form-control-solid mb-3 mb-lg-0 border border-gray-300 border-active active" placeholder="" value="" />
            <!--end::Input-->
        </div>
        <div class="fv-row mb-10">
            <!--begin::Label-->
            <label class="required fw-bold fs-6 mb-2">Contraseña</label>
            <!--end::Label-->

            <!--begin::Input-->
            <input type="password" name="password" class="form-control form-control-solid mb-3 mb-lg-0 border border-gray-300 border-active active" placeholder=""
                value="" />
            <!--end::Input-->
        </div>

        <div class="fv-row mb-10">
            <!--begin::Label-->
            <label class="required fw-bold fs-6 mb-2">Confirmar contraseña</label>
            <!--end::Label-->

            <!--begin::Input-->
            <input type="password" class="form-control form-control-solid mb-3 mb-lg-0 border border-gray-300 border-active active" placeholder=""
                value="" />
            <!--end::Input-->
        </div>

        <!--begin::Actions-->
        <button type="submit" class="btn btn-primary btn-hover-rise me-5">
            <span class="indicator-label">
                Registrar
            </span>
        </button>

        <a href="{{ route('users') }}">
            <button type="button" class="btn btn-danger btn-hover-rise me-5">
                <span class="indicator-label">
                    Cancelar
                </span>
                <span class="indicator-progress">
                    Please wait... <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                </span>
            </button>
        </a>
        <!--end::Actions-->
    </form>
    <!--end::Form-->
@endsection
