@extends('errors::minimal')

@section('title', 'Error 404')
{{-- @section('code') --}}
<!--begin::Main-->
<div class="d-flex flex-column flex-root">
    <!--begin::Authentication - 404 Page-->
    <div class="d-flex flex-column flex-center flex-column-fluid p-10">
        <!--begin::Illustration-->
        <img src="{{ asset('img/illustration-error404.png')}}" alt="" class="mw-100 mb-10 h-lg-450px" />
        <!--end::Illustration-->
        <!--begin::Message-->
        <h1 class="fw-bold mb-10" style="color: #A3A3C7">Oops! Estas perdido, te ayudaré a volver...</h1>
        <!--end::Message-->
        <!--begin::Link-->
        <a href="{{ route('dashboard') }}" class="btn btn-primary">Regresar al dashboard</a>
        <!--end::Link-->
    </div>
    <!--end::Authentication - 404 Page-->
</div>
<!--end::Main-->
