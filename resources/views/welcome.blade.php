@extends('layouts.welcome')

@section('content')
    <!--==================== HEADER ====================-->
    <header class="header" id="header">
        <nav class="nav container">
            <a href="#" class="nav__logo">
                <i class="inicio"></i> AR-Lab
            </a>

            <div class="nav__menu" id="nav-menu">
                <ul class="nav__list">
                    <li class="nav__item">
                        <a href="#home" class="nav__link active-link">Inicio</a>
                    </li>
                    <li class="nav__item">
                        <a href="#about" class="nav__link">Acerca de nosotros</a>
                    </li>
                    <li class="nav__item">
                        <a href="#products" class="nav__link">Productos</a>
                    </li>
                    <li class="nav__item">
                        <a href="#faqs" class="nav__link">FAQs</a>
                    </li>
                    <li class="nav__item">
                        <a href="#contact" class="nav__link">Contáctanos</a>
                    </li>
                </ul>

                <div class="nav__close" id="nav-close">
                    <i class="ri-close-line"></i>
                </div>
            </div>

            <div class="nav__btns">
                <!-- Theme change button -->
                <i class="ri-moon-line change-theme" id="theme-button"></i>

                <div class="nav__toggle" id="nav-toggle">
                    <i class="ri-menu-line"></i>
                </div>
            </div>
        </nav>
    </header>

    <main class="main">
        <!--==================== HOME ====================-->
        <section class="home" id="home">
            <div class="home__container container grid">
                <!--<img src="../resources/assets/img/home.png" alt="" class="home__img">-->
                {{-- <img src=" {{ asset('img/ARlogo.png') }} " alt="" class="home__img"> --}}
                <!--<p><a href=""><img src=" {{ asset('img/home.png') }} " alt="" class="home__img"/></a></p>-->

                <div class="home__data">
                    <h1 class="home__title">
                        Realidad <br> Aumentada
                    </h1>
                    <p class="home__description">
                        Atrévete, usa la realidad aumentada y aprende dinámicamente.
                    </p>
                    <a href="#" class="button primary icon solid fa-download">Descarga
                    </a>
                    <a href="#about" class="button button--flex">
                        Explora <i class="ri-arrow-right-down-line button__icon"></i>
                    </a>
                </div>

                <div class="home__social">
                    <span class="home__social-follow">Síguenos</span>

                    <div class="home__social-links">
                        <a href="{{ url('https://www.facebook.com/') }}" target="_blank" class="home__social-link">
                            <i class="ri-facebook-fill"></i>
                        </a>
                        <a href="{{ url('https://www.instagram.com/') }}" target="_blank" class="home__social-link">
                            <i class="ri-instagram-line"></i>
                        </a>
                        <a href="{{ url('https://twitter.com/') }}" target="_blank" class="home__social-link">
                            <i class="ri-twitter-fill"></i>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <!--==================== ABOUT ====================-->
        <section class="about section container" id="about">
            <div class="about__container grid">
                <!--<img src="../resources/assets/img/about.png" alt="" class="about__img">-->
                <img src="{{ asset('img/ARlogo2.png') }}" alt="" style="padding:50px">

                <div class="about__data">
                    <h2 class="section__title about__title">
                        Quiénes somos y <br> porque elegirnos
                    </h2>

                    <p class="about__description">
                        Somos la compañía número #1 trabajando con realidad aumentada.
                    </p>

                    <div class="about__details">
                        <p class="about__details-description">
                            <i class="ri-checkbox-fill about__details-icon"></i>
                            Nosotros siempre estamos innovando.
                        </p>
                        <p class="about__details-description">
                            <i class="ri-checkbox-fill about__details-icon"></i>
                            Nosotros te proporcionamos la tecnología.
                        </p>
                        <p class="about__details-description">
                            <i class="ri-checkbox-fill about__details-icon"></i>
                            Nosotros somos el futuro.
                        </p>
                        <p class="about__details-description">
                            <i class="ri-checkbox-fill about__details-icon"></i>
                            Tecnología 100% gratuita.
                        </p>
                    </div>

                    <!--<a href="#" class="button--link button--flex">
                                    Descargar ahora <i class="ri-arrow-right-down-line button__icon"></i>
                                </a>-->
                </div>
            </div>
        </section>

        <!--==================== STEPS ====================-->
        <section class="steps section container">
            <div class="steps__bg">
                <h2 class="section__title-center steps__title">
                    Pasos para empezar a <br> usar la aplicación.
                </h2>

                <div class="steps__container grid">
                    <div class="steps__card">
                        <div class="steps__card-number">01</div>
                        <h3 class="steps__card-title">App Móvil</h3>
                        <p class="steps__card-description">
                            Ejectuta la app móvil
                        </p>
                    </div>

                    <div class="steps__card">
                        <div class="steps__card-number">02</div>
                        <h3 class="steps__card-title">Objeto</h3>
                        <p class="steps__card-description">
                            Enfoca la imágen u objeto
                        </p>
                    </div>

                    <div class="steps__card">
                        <div class="steps__card-number">03</div>
                        <h3 class="steps__card-title">Aprende</h3>
                        <p class="steps__card-description">
                            Interactúa con la realidad aumentada
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <!--==================== PRODUCTS ====================-->
        <livewire:show-categories-welcome>
        <!--==================== PREGUNTAS ====================-->
        <section class="questions section" id="faqs">
            <h2 class="section__title-center questions__title container">
                Algunas preguntas en común <br> que hemos respondido
            </h2>

            <div class="questions__container container grid">
                <div class="questions__group">
                    <div class="questions__item">
                        <header class="questions__header">
                            <i class="ri-add-line questions__icon"></i>
                            <h3 class="questions__item-title">
                                ¿Qué puedo hacer si la aplicación Paso a Paso no abre?
                            </h3>
                        </header>

                        <div class="questions__content">
                            <p class="questions__description">
                                Elimine la caché de la aplicación o verifique que su sistema operativo sea superior a
                                Android 7.
                            </p>
                        </div>
                    </div>

                    <div class="questions__item">
                        <header class="questions__header">
                            <i class="ri-add-line questions__icon"></i>
                            <h3 class="questions__item-title">
                                ¿Qué hago si la aplicación no reconoce un objetivo?
                            </h3>
                        </header>

                        <div class="questions__content">
                            <p class="questions__description">
                                Trate de escanear el objetivo con una mayor cantidad de luz.
                            </p>
                        </div>
                    </div>

                    <div class="questions__item">
                        <header class="questions__header">
                            <i class="ri-add-line questions__icon"></i>
                            <h3 class="questions__item-title">
                                ¿Qué hago si la realidad aumentada no funciona?
                            </h3>
                        </header>

                        <div class="questions__content">
                            <p class="questions__description">
                                Lamentablemente la realidad aumentada solamente se encuentra disponible para dispositivos moviles
                                con Android 7 en adelante.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="questions__group">
                    <div class="questions__item">
                        <header class="questions__header">
                            <i class="ri-add-line questions__icon"></i>
                            <h3 class="questions__item-title">
                                ¿Cuando se actualizan los objetos?
                            </h3>
                        </header>

                        <div class="questions__content">
                            <p class="questions__description">
                                La base de datos de nuestros objetos se actualizará de manera periodica, esto nos permite tener
                                un mejor control de los objetivos y modelos en 3D que se agreguen.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--==================== CONTACTO ====================-->
        <section class="contact section container" id="contact">
            <div class="contact__container grid">
                <div class="contact__box">
                    <h2 class="section__title">
                        Comuníquese con nosotros a través de <br> cualquier medio de<br> información
                    </h2>

                    <div class="contact__data">
                        <div class="contact__information">
                            <h3 class="contact__subtitle">Llámanos para soporte instantáneo</h3>
                            <span class="contact__description">
                                <i class="ri-phone-line contact__icon"></i>
                                +52 983 123 4565
                            </span>
                        </div>

                        <div class="contact__information">
                            <h3 class="contact__subtitle">Escríbenos por medio de correo electrónico</h3>
                            <span class="contact__description">
                                <i class="ri-mail-line contact__icon"></i>
                                arlab@gmail.com
                            </span>
                        </div>
                    </div>
                </div>

                <form action="" class="contact__form">
                    <div class="contact__inputs">
                        <div class="contact__content">
                            <input type="email" placeholder=" " class="contact__input">
                            <label for="" class="contact__label">Correo</label>
                        </div>

                        <div class="contact__content">
                            <input type="text" placeholder=" " class="contact__input">
                            <label for="" class="contact__label">Asunto</label>
                        </div>

                        <div class="contact__content contact__area">
                            <textarea name="message" placeholder=" " class="contact__input"></textarea>
                            <label for="" class="contact__label">Mensaje</label>
                        </div>
                    </div>

                    <button class="button button--flex">
                        Enviar correo
                        <i class=""></i>
                    </button>
                </form>
            </div>
        </section>
    </main>

    <!--==================== FOOTER ====================-->
    <footer class="footer section">
        <div class="footer__container container grid">
            <div class="footer__content">
                <!--<a href="#" class="footer__logo">
                                <i class="ri-leaf-line footer__logo-icon"></i> AR-Lab
                            </a>-->
                <a href="#" class="footer__logo">
                    <i class=""></i> AR-Lab
                </a>

                <h3 class="footer__title">
                    Suscríbete para obtener actualizaciones, noticas o <br> eventos en tu correo electrónico.
                </h3>

                <div class="footer__subscribe">
                    <input type="email" placeholder="Ingresa tu correo" class="footer__input">

                    <button class="button button--flex footer__button">
                        Suscríbete
                        <i class="ri-arrow-right-up-line button__icon"></i>
                    </button>
                </div>
            </div>

            <div class="footer__content">
                <h3 class="footer__title">Nuestra dirección</h3>

                <ul class="footer__data">
                    <li class="footer__information">Chetumal, Quintana Roo - México</li>
                    <li class="footer__information">Ejemplo de calle - 77000</li>
                    <li class="footer__information">123-456-789</li>
                </ul>
            </div>

            <div class="footer__content">
                <h3 class="footer__title">Contáctanos</h3>

                <ul class="footer__data">
                    <li class="footer__information">+52 983 123 4567</li>

                    <div class="footer__social">
                        <a href="{{ url('https://www.facebook.com/') }}" class="footer__social-link">
                            <i class="ri-facebook-fill"></i>
                        </a>
                        <a href="{{ url('https://www.instagram.com/') }}" class="footer__social-link">
                            <i class="ri-instagram-line"></i>
                        </a>
                        <a href="{{ url('https://twitter.com/') }}" class="footer__social-link">
                            <i class="ri-twitter-fill"></i>
                        </a>
                    </div>
                </ul>
            </div>

        </div>

        <p class="footer__copy">&#169; AR-Lab. Todos los derechos están reservados.</p>
    </footer>
@endsection
