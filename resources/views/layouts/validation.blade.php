<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!--=============== FAVICON ===============-->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/your-logo.png') }}">

    <!--=============== CSRF Token ===============-->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--=============== FONTS ===============-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700" />

    <!--=============== CSS ===============-->
    <link href="{{ asset('css/style.bundle.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins.bundle.css') }}" rel="stylesheet">

    <title>Inicio de sesión</title>
</head>

<body>
    
        @yield('content')
    
</body>
<script src="{{ ('js/plugins.bundle.js')}}"></script>
<script src="{{ ('js/scripts.bundle.js')}}"></script>
<script src="{{ ('js/general.js')}}"></script>

</html>
