<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--=============== FAVICON ===============-->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/your-logo.png') }}">

    <!--=============== REMIX ICONS ===============-->
    <link href="{{ asset('css/remixicon.css') }}" rel="stylesheet">

    <!--=============== CSS ===============-->
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">

    <!--=============== PWA ===============-->
    <meta name="theme-color" content="#6777ef" />
    <link rel="apple-touch-icon" href="{{ asset('your-logo-512.png') }}">
    <link rel="manifest" href="{{ asset('/manifest.json') }}">

    <!-- iPhone X (1125px x 2436px) -->
    <link rel="apple-touch-startup-image" media="(device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3)"  href="{{ asset('/your-logo-72') }}">
    <!-- iPhone 8, 7, 6s, 6 (750px x 1334px) -->
    <link rel="apple-touch-startup-image" media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)"  href="{{ asset('/your-logo-96') }}">
    <!-- iPhone 8 Plus, 7 Plus, 6s Plus, 6 Plus (1242px x 2208px) -->
    <link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 736px) and (-webkit-device-pixel-ratio: 3)"  href="{{ asset('/your-logo-128') }}">
    <!-- iPhone 5 (640px x 1136px) -->
    <link rel="apple-touch-startup-image" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)"  href="{{ asset('/your-logo-152') }}">
    <!-- iPad Mini, Air (1536px x 2048px) -->
    <link rel="apple-touch-startup-image" media="(device-width: 768px) and (device-height: 1024px) and (-webkit-device-pixel-ratio: 2)" href="{{ asset('/your-logo-192') }}">
    <!-- iPad Pro 10.5" (1668px x 2224px) -->
    <link rel="apple-touch-startup-image" media="(device-width: 834px) and (device-height: 1112px) and (-webkit-device-pixel-ratio: 2)" href="{{ asset('/your-logo-384') }}">
    <!-- iPad Pro 12.9" (2048px x 2732px) -->
    <link rel="apple-touch-startup-image" media="(device-width: 1024px) and (device-height: 1366px) and (-webkit-device-pixel-ratio: 2)"href="{{ asset('/your-logo-512') }}">

    @livewireStyles

    <title>Paso a Paso</title>
</head>

<body>
    @yield('content')

    <!--=============== JS ===============-->
    <script src="{{ asset('js/scrollreveal.min.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="{{ asset('/sw.js') }}"></script>
    <script>
        if (!navigator.serviceWorker.controller) {
            navigator.serviceWorker.register("/sw.js").then(function(reg) {
                console.log("Service worker has been registered for scope: " + reg.scope);
            });
        }
    </script>
    @livewireScripts
</body>

</html>