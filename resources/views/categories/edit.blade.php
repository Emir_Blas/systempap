@extends('layouts.home')

@section('title', 'Categorias')

@section('title-nav', 'Editar categoria')

@section('content')
    <br><br><br><br><br>
    <ol class="breadcrumb text-muted fs-6 fw-bold">
        <li class="breadcrumb-item pe-3"><a href="{{ route('dashboard') }}" class="pe-3">Inicio</a></li>
        <li class="breadcrumb-item pe-3"><a href="{{ route('categories.index') }}" class="pe-3">Categorias</a></li>
        <li class="breadcrumb-item px-3 ">Editar categoria</li>
    </ol><br><br>
    <!--begin::Form-->
    <form class="form" action="{{ route('categories.update', ['category' => $category->id]) }}" method="POST"
        enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <!--begin::Input group-->
        <div class="fv-row mb-10">
            <!--begin::Label-->
            <label class="required fw-bold fs-6 mb-2">Nombre de la categoria</label>
            <!--end::Label-->

            <!--begin::Input-->
            <input type="text" name="category" class="form-control form-control-solid mb-3 mb-lg-0 border border-gray-300 border-active active" placeholder=""
                value="{{ $category->category }}" />
                
            <!--end::Input-->
        </div>
        <div class="fv-row mb-10">
            <!--begin::Label-->
            <label class="required fw-bold fs-6 mb-2">Estado de la categoria</label>
            <!--end::Label-->

            <!--begin::Select-->
            <select name="statu" class="form-select " data-control="select2" data-placeholder="Select an option">
                <option value="{{$category->statu_id}}">{{$category->statu->statu}}</option>
                @foreach($status as $statu)
                    @if($category->statu_id == $statu->id)
                        
                    @else
                        <option value="{{$statu->id}}">{{$statu->statu}}</option>
                    @endif
                @endforeach
            </select>
            <!--end::Select-->
        </div>
        <div class="fv-row mb-10">
            <!--begin::Label-->
            <label class="required fw-bold fs-6 mb-2">URL Imagen en linea</label>
            <!--end::Label-->

            <!--begin::Input-->
            <input type="text" name="url" class="form-control form-control-solid mb-3 mb-lg-0 border border-gray-300 border-active active" placeholder=""
                value="{{$category->url}}" />
            <!--end::Input-->
        </div>
        <!--begin::Actions-->
        <button type="submit" class="btn btn-primary btn-hover-rise me-5">
            <span class="indicator-label">
                Actualizar
            </span>
        </button>

        <a href="{{ route('categories.index') }}">
            <button type="button" class="btn btn-danger btn-hover-rise me-5">
                <span class="indicator-label">
                    Cancelar
                </span>
                <span class="indicator-progress">
                    Please wait... <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                </span>
            </button>
        </a>
        <!--end::Actions-->
    </form>
    <!--end::Form-->
@endsection