<!-- Fade In Modal -->
<div class="modal fade" id="deletedata{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="deletedata" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Eliminando {{$item->name}}</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    <p>¿Realmente desea eliminar de manera permanente este registro del sistema?</p>
                </div>
                <div class="alert alert-danger" role="alert">
                    Eliminar un registro en el modulo de Administración, puede ocasionar un mal funcionamiento en el sistema.
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cancelar</button>
                <form action="{{ route('objects.destroy', ['id'=>$item->id]) }}" method="POST">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-alt-success">
                        <i class="fa fa-check"></i> Aceptar
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END Fade In Modal -->