<!-- Fade In Modal -->
<div class="modal fade" tabindex="-1" id="imagedata{{$item->id}}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{$item->name}}</h3>

                <!--begin::Close-->
                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    <span class="svg-icon svg-icon-1"></span>
                </div>
                <!--end::Close-->
            </div>

            <div class="modal-body">
                <p>Escanea y prueba</p>
                <img src="{{asset($item->image)}}" alt="{{ $item->id }}" style="width:100%; height:100%; ">
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- <div class="modal fade" id="imagedata{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="deletedata" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Imagen sobre {{$item->name}}</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    <p>Imagen relacionada al objeto seleccionado.</p>
                </div>
                {{-- <div class="block-content">
                    {{$item->description}}
                </div> --}}
                <div class="block-content">
                </div>
                <div class="alert alert-warning" role="alert">
                    <img src="{{asset($item->image)}}" alt="{{ $item->id }}" style="width:465px; height:465px; ">
                </div>
            </div>
            {{-- <div class="modal-footer">
                <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cancelar</button>
                <form action="{{ route('report.update', ['id'=>$item->id]) }}" method="POST">
                    @method('PUT')
                    @csrf
                    <button type="submit" class="btn btn-alt-success">
                        <i class="fa fa-check"></i> Aceptar
                    </button>
                </form>
            </div> --}}
        </div>
    </div>
</div> -->
<!-- END Fade In Modal -->