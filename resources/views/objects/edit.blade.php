@extends('layouts.home')

@section('title', 'Objetos')

@section('title-nav', 'Editar objeto')

@section('content')
    <br><br><br><br><br>
    <ol class="breadcrumb text-muted fs-6 fw-bold">
        <li class="breadcrumb-item pe-3"><a href="{{ route('dashboard') }}" class="pe-3">Inicio</a></li>
        <li class="breadcrumb-item pe-3"><a href="{{ route('objects') }}" class="pe-3">Objetos</a></li>
        <li class="breadcrumb-item px-3 ">Editar objeto</li>
    </ol><br><br>
    <!--begin::Form-->
    <form class="form" action="{{ route('objects.update', ['id' => $objects->id]) }}" method="POST"
        enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <!--begin::Input group-->
        <div class="fv-row mb-10">
            <!--begin::Label-->
            <label class="required fw-bold fs-6 mb-2">Nombre del objeto</label>
            <!--end::Label-->

            <!--begin::Input-->
            <input type="text" name="name" class="form-control form-control-solid mb-3 mb-lg-0 border border-gray-300 border-active active" placeholder=""
                value="{{ $objects->name }}" />
            <!--end::Input-->
        </div>
        <div class="fv-row mb-10">
            <!--begin::Label-->
            <label class="required fw-bold fs-6 mb-2">Descripción del objeto</label>
            <!--end::Label-->

            <!--begin::Input-->
            <input type="text" name="description" class="form-control form-control-solid mb-3 mb-lg-0 border border-gray-300 border-active active" placeholder=""
                value="{{ $objects->description }}" />
            <!--end::Input-->
        </div>
        <div class="fv-row mb-10 row">
            <div class="col-md-6">
                <!--begin::Label-->
                <label class="required fw-bold fs-6 mb-2">Categoria del objeto</label>
                <!--end::Label-->

                <!--begin::Select-->
                <select name="category" class="form-select " data-control="select2" data-placeholder="Select an option">
                <option value="{{$objects->category_id}}">{{$objects->category->category}}</option>
                    @foreach($categories as $category)
                        @if($objects->category_id == $category->id)

                        @else
                            <option value="{{$category->id}}">{{$category->category}}</option>
                        @endif
                    @endforeach
                </select>
                <!--end::Select-->
            </div>
            <div class="col-md-6">
                <!--begin::Label-->
                <label class="required fw-bold fs-6 mb-2">Estado del objeto</label>
                <!--end::Label-->

                <!--begin::Select-->
                <select name="statu" class="form-select " data-control="select2" data-placeholder="Select an option">
                    <option value="{{$objects->statu_id}}">{{$objects->statu->statu}}</option>
                    @foreach($status as $statu)
                        @if($objects->statu_id == $statu->id)
                        
                        @else
                            <option value="{{$statu->id}}">{{$statu->statu}}</option>
                        @endif
                    @endforeach
                </select>
                <!--end::Select-->
            </div>
        </div>
        <div class="fv-row mb-10">
            <!--begin::Label-->
            <label class="required fw-bold fs-6 mb-2">URL API</label>
            <!--end::Label-->

            <!--begin::Input-->
            <input type="text" name="url_api" class="form-control form-control-solid mb-3 mb-lg-0 border border-gray-300 border-active active" placeholder=""
                value="{{$objects->url_api}}" />
            <!--end::Input-->
        </div>
        <!--begin::Image input-->
        <div class="image-input image-input-outline" data-kt-image-input="true"
            style="background-image: url('/assets/media/avatars/blank.png')">
            <!--begin::Image preview wrapper-->
            <div class="image-input-wrapper w-125px h-125px" style="background-image: url('{{ $objects->image }}')">
            </div>
            <!--end::Image preview wrapper-->

            <!--begin::Edit button-->
            <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-white shadow"
                data-kt-image-input-action="change" data-bs-toggle="tooltip" data-bs-dismiss="click" title="Asignar imagen">
                <i class="bi bi-pencil-fill fs-7"></i>

                <!--begin::Inputs-->
                <input type="file" name="image" accept=".png, .jpg, .jpeg" />
                <input type="hidden" name="image" />
                <!--end::Inputs-->
            </label>
            <!--end::Edit button-->

            <!--begin::Cancel button-->
            <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-white shadow"
                data-kt-image-input-action="cancel" data-bs-toggle="tooltip" data-bs-dismiss="click"
                title="Cancelar imagen">
                <i class="bi bi-x fs-2"></i>
            </span>
            <!--end::Cancel button-->

            <!--begin::Remove button-->
            <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-white shadow"
                data-kt-image-input-action="remove" data-bs-toggle="tooltip" data-bs-dismiss="click"
                title="Eliminar imagen">
                <i class="bi bi-x fs-2"></i>
            </span>
            <!--end::Remove button-->
        </div>
        <!--end::Image input-->

        <div class="fv-row mb-10">
            <!--begin::Label-->
            <label class="required fw-bold fs-6 mb-2">URL Imagen en web</label>
            <!--end::Label-->

            <!--begin::Input-->
            <input type="text" name="url_image" class="form-control form-control-solid mb-3 mb-lg-0 border border-gray-300 border-active active" placeholder=""
                value="{{$objects->url_image}}" />
            <!--end::Input-->
        </div>
        <br><br>

        <!--begin::Actions-->
        <button type="submit" class="btn btn-primary btn-hover-rise me-5">
            <span class="indicator-label">
                Actualizar
            </span>
        </button>

        <a href="{{ route('objects') }}">
            <button type="button" class="btn btn-danger btn-hover-rise me-5">
                <span class="indicator-label">
                    Cancelar
                </span>
                <span class="indicator-progress">
                    Please wait... <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                </span>
            </button>
        </a>
        <!--end::Actions-->
    </form>
    <!--end::Form-->
@endsection
