<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>AR-Lab - Restablecer contraseña</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- favicon -->
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('') }}">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.1/font/bootstrap-icons.css"> --}}
    <!-- Style -->
    <link href="{{ asset('css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    
</head>
<body id="kt_body" class="bg-body">
    <!--begin::Main-->
    <div class="d-flex flex-column flex-root">
        <!--begin::Authentication - Signup Free Trial-->
        <div class="d-flex flex-column flex-xl-row flex-column-fluid">
            <!--begin::Aside-->
            <div class="d-flex flex-column flex-lg-row-fluid">
                <!--begin::Wrapper-->
                <div class="d-flex flex-row-fluid flex-center p-10">
                    <!--begin::Content-->
                    <div class="d-flex flex-column">
                        <!--begin::Logo-->
                        {{-- <a href="" class="mb-15">
                            <img alt="Logo" src="{{ asset('') }}" class="h-40px" />
                        </a> --}}
                        <!--end::Logo-->
                        <!--begin::Title-->
                        <h1 class="text-dark fs-2x mb-3">Bienvenido, Administrador!</h1>
                        <!--end::Title-->
                        <!--begin::Description-->
                        <div class="fw-bold fs-4 text-gray-400 mb-10">Atrévete, usa la realidad aumentada y
                        <br />aprende dinámicamente.</div>
                        <!--begin::Description-->
                    </div>
                    <!--end::Content-->
                </div>
                <!--end::Wrapper-->
                <!--begin::Illustration-->
                <div class="d-flex flex-row-auto bgi-no-repeat bgi-position-x-center bgi-size-contain bgi-position-y-bottom min-h-200px min-h-xl-450px mb-xl-10" style="background-image: url('../../img/illustration-login.png')"></div>
                <!--end::Illustration-->
            </div>
            <!--begin::Aside-->
            <!--begin::Content-->
            <div class="flex-row-fluid d-flex flex-center justfiy-content-xl-first p-10">
                <!--begin::Wrapper-->
                <div class="d-flex flex-center p-15 shadow bg-body rounded w-100 w-md-550px mx-auto ms-xl-20">
                    <!--begin::Form-->
                    <form method="POST" action="{{ route('password.update') }}" class="form" id="kt_free_trial_form">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">
                        <!--begin::Heading-->
                        <div class="text-center mb-10">
                            <!--begin::Title-->
                            <h1 class="text-dark mb-3">Restableciendo contraseña</h1>
                            <!--end::Title-->
                            <!--begin::Link-->
                            <div class="text-gray-400 fw-bold fs-4">Por favor registra una nueva contraseña</div>
                            {{-- <a href="#" class="link-primary fw-bolder">FAQ</a>.</div> --}}
                            <!--end::Link-->
                        </div>
                        <!--begin::Heading-->
                        <!--begin::Input group-->
                        <div class="fv-row mb-10">
                            <label for="email" class="form-label fw-bolder text-dark fs-6">Correo electrónico</label>
                            <input class="form-control form-control-solid" type="email" placeholder="" name="email" value="{{ $email ?? old('email') }}" autocomplete="off" />
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="mb-7 fv-row" data-kt-password-meter="true">
                            <!--begin::Wrapper-->
                            <div class="mb-1">
                                <!--begin::Label-->
                                <label for="password" class="form-label fw-bolder text-dark fs-6">Contraseña</label>
                                <!--end::Label-->
                                <!--begin::Input wrapper-->
                                <div class="position-relative mb-3">
                                    <input class="form-control form-control-solid @error('password') is-invalid @enderror" id="password" type="password" placeholder="" name="password" autocomplete="off" required autocomplete="new-password"/>
                                
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <!--end::Wrapper-->
                        </div>
                        <div class="mb-7 fv-row" data-kt-password-meter="true">
                            <!--begin::Wrapper-->
                            <div class="mb-1">
                                <!--begin::Label-->
                                <label for="password-confirm" class="form-label fw-bolder text-dark fs-6">Confirmar Contraseña</label>
                                <!--end::Label-->
                                <!--begin::Input wrapper-->
                                <div class="position-relative mb-3">
                                    <input class="form-control form-control-solid" id="password-confirm" type="password" placeholder="" name="password_confirmation" autocomplete="off" required autocomplete="new-password"/>
                                </div>
                            </div>
                            <!--end::Wrapper-->
                        </div>
                        <!--end::Input group=-->
                        <!--begin::Row-->
                        <div class="text-center pb-lg-0 pb-8">
                            <button type="submit" class="btn btn-lg btn-primary fw-bolder">
                                <span class="indicator-label">Iniciar sesión</span>
                                <span class="indicator-progress">Please wait...
                                <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                            </button><br><br>
                        </div>
                        <!--end::Row-->
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Wrapper-->
            </div>
            <!--end::Right Content-->
        </div>
        <!--end::Authentication - Signup Free Trial-->
    </div>
    <!--end::Main-->
    <!--begin::Javascript-->
    <!--begin::Global Javascript Bundle(used by all pages)-->
    <script src="{{asset('js/plugins.bundle.js')}}"></script>
    <script src="{{asset('js/scripts.bundle.js')}}"></script>
    <!--end::Global Javascript Bundle-->
    <!--begin::Page Custom Javascript(used by this page)-->
    {{-- <script src="{{asset('js/free-trial.js')}}"></script> --}}
    <!--end::Page Custom Javascript-->
    <!--end::Javascript-->
</body>
</html>
