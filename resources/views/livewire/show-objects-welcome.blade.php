<div>
    <section class="product section container" id="products">
        <h2 class="section__title-center">
            Catálogo de {{$category->category}} <br> disponibles
        </h2>
        <p class="product__description">
            {{$category->category}} que se encuentrán disponibles para interactuar con la aplicación Paso a Paso.
        </p>
        <br>
        <center>
            <button class="button primary icon solid" wire:click="$set('category_id', 0)">Regresar a las categorias</button>
            <br><br>
            <div class="footer__subscribe">
                <input type="text" wire:model.debounce.300ms="search" placeholder="Buscar {{$category->category}}" class="footer__input">
            </div>
        </center>
        <div class="product__container grid">
            @foreach ($objects as $item)
            <article class="product__card">
                <h3 class="product__title" style="text-align: center;">{{ $item->name }}</h3>
                <center>
                    <button type="button" class="btn" data-bs-toggle="modal" data-bs-target="#imagedata{{$item->id}}">
                        <img src="{{ asset($item->image) }}" alt="{{ $item->id }}" style="width: 100px; height:100px;">
                    </button>
                </center>
                <span class="product__price" style="text-align: center;">¡Escanea y disfruta!</span>
            </article>

            <div class="modal fade" id="imagedata{{$item->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">{{$item->name}}</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <p class="text-center">Escanea y prueba</p>
                            <img src="{{asset($item->image)}}" alt="{{ $item->id }}" style="width:100%; height:100%; ">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            @endforeach
        </div>
        {{$objects->links()}}
    </section>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</div>