@if(!$objects)
<div>
    <section class="product section container" id="products">

        <h2 class="section__title-center">
            Catálogo de categorias <br> disponibles
        </h2>
        <p class="product__description">
            Aquí algunas categorias que se encuentrán disponibles para interactuar con la aplicación Paso a Paso.
        </p>
        <br>
        <center>
            <div class="footer__subscribe">
                <input type="text" wire:model.debounce.300ms="search" placeholder="Buscar categoria" class="footer__input">
            </div>
        </center>
        <div class="product__container grid">
            @foreach ($categories as $item)
            <article class="product__card">
                <h3 class="product__title" style="text-align: center;">{{ $item->category }}</h3>
                <center>
                    <img src="{{ asset($item->url) }}" alt="{{ $item->id }}" wire:click="$set('category_id', '{{$item->id}}')">
                </center>
                <span class="product__price" style="text-align: center;">¡Descubre nuestro catálogo de {{$item->category}}!</span>
            </article>
            @endforeach
        </div>
        {{$categories->links()}}
    </section>
</div>
@else
    <livewire:show-objects-welcome>
@endif