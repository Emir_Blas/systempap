<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objects', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->string('description');
            $table->string('image')->unique();
            $table->string('url_image')->unique();
            $table->string('url_api')->unique();
            $table->bigInteger('category_id')->unsigned();
			$table->foreign('category_id')->references('id')->on('categories');
            $table->bigInteger('statu_id')->unsigned();
			$table->foreign('statu_id')->references('id')->on('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('objects');
    }
}
