<?php

namespace App\Http\Controllers;

use App\Models\Objects;
use App\Models\Statu;
use App\Models\Category;
use Illuminate\Http\Request;

class ObjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $objects = Objects::all();
		return view('objects.index')->with(compact('objects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('statu_id', 'Like', 1)->get();
        $status = Statu::all();
        return view('objects.create')->with(compact('status', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $objects = new objects;
        $objects->name = $request->name;
        $objects->description = $request->description;
        $objects->category_id = $request->category;
        $objects->statu_id = $request->statu;
        $objects->url_image = $request->url_image;
        $objects->url_api = $request->url_api;
        if( $request->hasfile('image')){
            $file = $request->file('image');
            $destionationPath = 'images/objects/';
            $filename = time() . '-' . $file->getClientOriginalName();
            $uploadSuccess = $request->file('image')->move($destionationPath, $filename);
            $objects->image = $destionationPath . $filename;
        }
        if($objects->save()){
            return redirect()->route('objects')->with('msj_add','El objeto ha sido guardado exitosamente');
        }else{
            return redirect()->route('objects')->with('error_msj','El registro de objeto ha fallado de forma inesperada.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Objects  $objects
     * @return \Illuminate\Http\Response
     */
    public function show(Objects $objects)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Objects  $objects
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $objects = Objects::find($id);
        $categories = Category::where('statu_id', 'Like', 1)->get();
        $status = Statu::all();
        return view('objects.edit', compact('objects','status', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Objects  $objects
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objects = Objects::find($id);
        $objects->name = $request->name;
        $objects->description = $request->description;
        $objects->category_id = $request->category;
        $objects->statu_id = $request->statu;
        $objects->url_image = $request->url_image;
        $objects->url_api = $request->url_api;
        if( $request->hasfile('image')){

            unlink($objects->image);
            
            $file = $request->file('image');
            $destionationPath = 'images/objects/';
            $filename = time() . '-' . $file->getClientOriginalName();
            $uploadSuccess = $request->file('image')->move($destionationPath, $filename);
            $objects->image = $destionationPath . $filename;
        }
        if($objects->save()){
            return redirect()->route('objects');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Objects  $objects
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Objects::find($id)->delete();
		return redirect()->route('objects')->with('msj_delete','El objeto ha sido eliminado exitosamente');;
    }
}
