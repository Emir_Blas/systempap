<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use App\Models\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class SMSController extends Controller
{
    public function send(Request $request){
        if (request()->isMethod('POST')) {
            $user = request('telefono');
            // $user = $this->validate($request, ['phone' => 'required|phone']);
            // $response = $this->broker()->sendResetLink(
            //     $request->only('phone')
            // );
            $from = getenv("TWILIO_FROM");
            //open connection

            $ch = curl_init();
    
            //set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_USERPWD, getenv("TWILIO_SID").':'.getenv("TWILIO_TOKEN"));
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            curl_setopt($ch, CURLOPT_URL, sprintf('https://api.twilio.com/2010-04-01/Accounts/'.getenv("TWILIO_SID").'/Messages.json', getenv("TWILIO_SID")));
            curl_setopt($ch, CURLOPT_POST, 3);
            curl_setopt($ch, CURLOPT_POSTFIELDS, 'To='.$user.'&From='.$from.'&Body='.'http://localhost/SystemPaP/public/password/reset/03fe720557293e9f644ddf7803c5f34dd35cba18a0d52ceae9ef7877d7ea58f6?email=josue.aguilar225%40gmail.com');
    
            // execute post
            $result = curl_exec($ch);
            $result = json_decode($result);
    
            // close connection
            curl_close($ch);
            //Sending message ends here
            return [$result];
        }
        return view("auth.passwords.sms");
    }
}
