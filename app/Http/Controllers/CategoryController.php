<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Statu;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('categories.index')->with(compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $status = Statu::all();
        return view('categories.create')->with(compact('status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new Category();
        $category->category = $request->category;
        $category->statu_id = $request->statu;
        $category->url = $request->url;
        if($category->save()){
            return redirect()->route('categories.index')->with('msj_add','La categoria ha sido guardado exitosamente');
        }else{
            return redirect()->route('categories.index')->with('error_msj','El registro de la categoria ha fallado de forma inesperada.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $status = Statu::all();
        $category = Category::find($id);
        return view('categories.edit')->with(compact('status','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        $category->category = $request->category;
        $category->statu_id = $request->statu;
        $category->url = $request->url;
        if($category->save()){
            return redirect()->route('categories.index')->with('msj_add','La categoria ha sido modificada exitosamente');
        }else{
            return redirect()->route('categories.index')->with('error_msj','La modificación de la categoria ha fallado de forma inesperada.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
