<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Category;
use App\Models\Objects;

class ShowCategoriesWelcome extends Component
{
    use WithPagination;

    public $search, $category_id;

    public function updating()
    {
        $this->resetPage();
    }

    public function render()
    {
        if ($this->category_id != 0) {
            $this->search = "";
            $categories = null;
            $category = Category::find($this->category_id);
            $objects = Objects::where('name', 'LIKE', '%' . $this->search . '%')
                ->where('category_id', 'LIKE', $this->category_id)
                ->where('statu_id', 'LIKE', 1)
                ->SimplePaginate(6);
            return view('livewire.show-objects-welcome')->with(compact('objects', 'categories', 'category'));
        } else {
            $objects = null;
            $categories = Category::where('category', 'LIKE', '%' . $this->search . '%')
                ->where('statu_id', 'LIKE', 1)
                ->SimplePaginate(6);
            return view('livewire.show-categories-welcome')->with(compact('categories', 'objects'));
        }
    }
}
