<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $fillable = ['category', 'statu_id'];

    public function statu()
    {
        return $this->belongsTo(Statu::class, 'statu_id', 'id');
    }
}
