<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Objects extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'description', 'image', 'url_image', 'url_api', 'category_id', 'statu_id',];

    public function statu()
    {
        return $this->belongsTo(Statu::class, 'statu_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
}
