<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ObjectsController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WelcomeController;
use App\Http\Controllers\SMSController;
use App\Http\Controllers\CategoryController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Routes for Auth
Auth::routes([ 'verify' => true ]);

//Routes for reset password

// Route::get('/password/reset', [ForgotPasswordController::class, 'index'])->name('pwd-reset');


//Routes for welcome
Route::get('/', [WelcomeController::class, 'index'])->name('welcome');

//Routes for dashboard
Route::get('/dashboard', [HomeController::class, 'index'])->name('dashboard');

Route::any('send-sms', [SMSController::class, 'send']);

//Routes for objects
Route::get('/objects', [ObjectsController::class, 'index'])->name('objects');
Route::get('/objects-add', [ObjectsController::class, 'create'])->name('objects.create');
Route::post('/objects-add-store', [ObjectsController::class, 'store'])->name('objects.store');
Route::get('/objects-edit-{id}', [ObjectsController::class, 'edit'])->name('objects.edit');
Route::put('/objects-update-{id}', [ObjectsController::class, 'update'])->name('objects.update');
Route::delete('/objects-delete-{id}', [ObjectsController::class, 'destroy'])->name('objects.destroy');
// End routes

//Routes for users
Route::get('/users', [UserController::class, 'index'])->name('users');
Route::get('/users-add', [UserController::class, 'create'])->name('users.create');
Route::post('/users-add-store', [UserController::class, 'store'])->name('users.store');
Route::get('/users-edit-{id}', [UserController::class, 'edit'])->name('users.edit');
Route::put('/users-update-{id}', [UserController::class, 'update'])->name('users.update');
Route::delete('/users-delete-{id}', [UserController::class, 'destroy'])->name('users.destroy');
// End routes

//Route for categories
Route::resource('categories', CategoryController::class)->names('categories');